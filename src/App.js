import React from 'react';
import './firebase';
import { Provider } from 'mobx-react';
import { UserStore } from './user-store'
import { Root } from './root';

function App() {
  return (
    <Provider userStore={new UserStore()}>
      <Root />
    </Provider>
  );
}

export default App;
