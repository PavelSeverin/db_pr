import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Input, Button, Typography } from '@material-ui/core';
import { observable } from 'mobx';

@inject('userStore')
@observer
class Login extends Component {
  @observable email = '';
  @observable pass = '';
  @observable isSigningIn = false;
  @observable errorMsg = '';

  onSignIn = async () => {
    const { userStore } = this.props;
    try {
      this.isSigningIn = true;
      this.errorMsg = '';
      await userStore.signIn(this.email, this.pass);
    } catch (err) {
      this.errorMsg = err.message;
    } finally {
      this.isSigningIn = false;
    }
  };

  render() {
    if (this.isSigningIn) return 'Signing in..';

    return (
      <div style={{ maxWidth: 360 }}>
        <Input
          placeholder={'Email'}
          value={this.email}
          onChange={(ev) => (this.email = ev.target.value)}
          fullWidth
          type="email"
          name="email"
        />
        <Input
          placeholder={'Password'}
          value={this.pass}
          onChange={(ev) => (this.pass = ev.target.value)}
          fullWidth
          type="password"
          name="password"
        />
        <br />
        {this.errorMsg && (
          <Typography color="error">{this.errorMsg}</Typography>
        )}
        <br />
        <Button onClick={this.onSignIn}>Sign in</Button>
      </div>
    );
  }
}

export { Login };
